# Public Infra Config

This repository consists of our Tailscale ACLs (privacy notice: don't worry, IP addresses are internal to our tailnet), our IaC stuff for Terraform, among other things in the infrastructure side of Recap Time squad.

## FAQ

### Tailscale: What's your tailnet?

Our tailnet is `recaptime.org.github` which translates into our GitHub org with same slug here in GitLab SaaS. In order to access other devices, somebody would need and ask @ajhalili2006 to:

a. add them to the `RecapTime` GitHub org as part of `external` team - by default they only have access to public repos in read mode on Git branches, sign in to Tailscale and add their device; or
b. send them an link to share access with one of devices within our tailnet, specifically personal devices registered there (including mine) to use your existing tailnet instead[^2]

### Tailscale: On paid team plan?

Nah, mate. We're on GitHub Community plan which means tl;dr **gives more for free** on OSS projects and families/friends:

* Up to 25 users, with 5 devices per user (25 users max X 5 devices per user = up to 145 devices)
* 2 admin users for device and ACL management
* 1 subnet router, in case you want to connect your printer to the tailnet without hiccups
* 2 unique users on ACL policies for fine-grained controls (rules that applies to everyone, or by machine tag and/or user group are free)
* of course, unlimited ACL tags

If we ever go to the Team plan, each user costs US$60 (PHP 3045.11 when converted[^1]) on yearly basis or US$5 (PHP 253.76 when converted[^1]) plus additional $5 per 10 devices (free if below 5 devices X number of user seats used).

### Tailscale: If I opt to join your tailnet by going to GitHub org invite route, do I need 2FA?

Yes. We require org members to enable 2FA ASAP to keep your account protected. IF you prefer not to enable 2FA, please go to machine sharing route instead.

[^1]: Currency convensions are calucated based on mid-market rates as of 2021-10-27 16:00 UTC. For up-to-date conversions, search `usd to php` in your search engine, [particularly DDG](https://duckduckgo.com/?q=usd+to+php&atb=v296-4&ia=currency).
[^2]: <https://tailscale.com/kb/1084/sharing>
